## What is this repository for?

This repository is for the use of all Deloitte Analytics members. It contains a nice presentation template, built in R, with the presentation output in the browser. It also contains some example plots, with nice graphics and look.

## How do I get set up?


Download / clone the whole repository.

Insert your slides into graphics-ird.Rmd, located in the parent directory.

The slides support both R-code, R output and text / images / videos / animations.

Each slides starts with a heading, which is initialised as # your_slide_heading,

or, with a double hash, as ## your_slide_ heading. See the section "Headings" for details.

After that, open up build.R, and insert the correct full file path to graphics-ird.Rmd. And graphics-ird.html. For example, it might look like this:

render('C:/Users/your_user_name/Downloads/graphics-taster-master/graphics-ird.Rmd').

browseURL("C:/Users/your_user_name/Downloads/graphics-taster-master/graphics-ird.html").

Once you do it once, it's fine.

To run the presentation, just Run build.R (select all lines and click Run or Ctrl + Enter).

This will create a HTML file, graphics-ird.html located in the same folder, and open it up automatically.

Some instructions / set up is presented below. Visit http://lab.hakim.se/reveal-js/#/ for more complex set-ups like background colour, video, etc.

## Instructions / Details 

### Headings:

Put a hash ("#") before a section heading, through which you can switch with right/left arrows.

Put a double hash ("##") before a slide heading in that section. You can scroll through these with an up/down arrow.

If you feel confused, just use PgDn, this will just scroll through the slides in order.

Examples:


```
#!R

# Cars

## BMW

## Mercedes

## Fiat

## Lada

# Planes

## Airbus

## Boeing

## Tu

```


### R-code:



Shall you wish to insert R-code into your slide:

After the heading / title, put in your R-code within:








```
#!R

 ```{r}


 ```
```





This will output whatever you generate by the R-code onto the slide.

Watch-out that you don't have too much content in one-slide - the slides are un-scrollable.

If you want to display the R-code used, insert your code within:





```
#!R

```{r echo = TRUE}


 ```
```




Example:



```
#!R


## Plot {data-transition="fade-in"}

```{r}

require(stats) # for lowess, rpois, rnorm
plot(cars)
lines(lowess(cars))

plot(sin, -pi, 2*pi) # see ?plot.function

## Discrete Distribution Plot: (it ignores the hashes and double hashes here, treats them as a comment)
plot(table(rpois(100, 5)), type = "h", col = "red", lwd = 10,
     main = "rpois(100, lambda = 5)")

 ```

```


### Transitions:

Next to the title, in the same line, add your transition: 
```
#!R

{data-transition="_____"}
```


Example transition: 


```
#!R

## Heading of a slide {data-transition="fade-out"}

## Another Heading of a slide {data-transition="fade-in}

# Heading {data-transition="fade"}

```

The default transition is the rotation of a cube - you don't have to specify anything if you want to use it.

Alternatives: non, fade, slide, convex, concave, zoom.


### Titles and Themes:

You can add a title, date and author to your slides by inserting them in the relevant place at the very start of the file.

Example:


```
#!R

---
title: "Statistical graphics for communicating"
author: "Peter Ellis"
date: "June 2016"
output:
  revealjs::revealjs_presentation:
    fig_height: 6
    theme: sky
    includes:
      in_header: header.css
 ---

```

Notice that output: down is necessary to keep.

You can also change the theme - from sky to:

black, white, league, sky, beige, simple, serif, blood, night, moon, solarized.

Recommended are: white, solarized, moon. But you can edit everything in the css or per slide.



### R Setup:


After the initial title, you have the option of loading the required libraries, data, etc.

Example:


```
#!R

```{r setup, include=FALSE}
stripcolour <- "grey93"
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, message = FALSE, cache = TRUE)
library(MASS)
library(dplyr)
library(RODBC)
library(car) # for vif()
library(GGally) # for ggpairs
library(ggseas)
source("helpers.R")
theme_set(theme_solarized(base_family = "Calibri") +
    theme(panel.background = element_blank(),
          strip.background = element_rect(colour = NA, fill = stripcolour)))

load("data/nzis.rda")
load("data/AreaUnits2013.rda")
 ```


```



### Images / Animations / Gifs:

In the slide, insert an image / animation / gif with the following code:


```
#!R

![image_name](images/image_location.png)

```

Inserting the correct image directory, of course!











## Who do I talk to? 

Talk to Gleb Dzhus (gdzhus@deloitte.ie), Conor Burke (coburke@deloitte.ie), or visit this website, which is the source: http://www.r-bloggers.com/presentation-slides-on-using-graphics/